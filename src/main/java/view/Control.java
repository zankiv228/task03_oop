package view;

/**
 * @version 1.0 Control
 */
@FunctionalInterface
public interface Control {
    void print();
}
