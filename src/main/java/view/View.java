package view;

import controller.Controlable;
import controller.Controller;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

/**
 * @version 1.0 View
 * View graphics the program
 */
public class View {
    private Map<String, Control> controlMap;
    private LinkedList<String> menu;
    private Controlable controller;

    /**
     * Constructor filling arrayLists
     */
    public View() {
        controller = new Controller();

        controlMap = new LinkedHashMap<>();
        controlMap.put("1", this::pickStones);
        controlMap.put("2", this::viewNecklace);
        controlMap.put("3", this::calculatePriceOfStones);
        controlMap.put("4", this::sortPriceOfStones);
        controlMap.put("5", this::sortByTransparency);

        menu = new LinkedList<>();
        menu.add("----------------------------------------------");
        menu.add("1 — pick stones for necklace");
        menu.add("2 — view necklace");
        menu.add("3 — calculate price of stones {$}");
        menu.add("4 — sort price of stones");
        menu.add("5 — sort transparency of stones by your range");
        menu.add("Exit — exit from this application");
        menu.add("----------------------------------------------");
    }

    /**
     * Pick Stones
     */
    private void pickStones() {
        controller.pickStones();
    }

    /**
     * Creating Necklace
     */
    private void viewNecklace() {
        controller.printInfoOfNecklace();
    }

    /**
     * Calculate price of all stones
     */
    private void calculatePriceOfStones() {
        controller.calculatePrices();
    }

    /**
     * Sort price
     */
    private void sortPriceOfStones() {
        controller.sortPriceOfStones();
    }

    /**
     * Sort by transparency
     */
    private void sortByTransparency() {
        controller.sortByTransparency();
    }

    /**
     * Show menu the program
     */
    private void showMenu() {
        for (String str : menu) {
            System.out.println(str);
        }
    }

    /**
     * method for start the program
     */
    public void start() {
        Scanner scanner = new Scanner(System.in);
        String answer;

        System.out.println("Welcome!");
        do {
            showMenu();
            answer = scanner.nextLine().toUpperCase();
            try {
                controlMap.get(answer).print();
            } catch (Exception e) {
                if (!answer.equals("EXIT")) {
                    System.out.println("Something went wrong");
                }
            }
        } while (!answer.equals("EXIT"));
    }
}
