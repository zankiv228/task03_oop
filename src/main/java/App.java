import view.View;

/**
 * @author Oleh Zankiv
 * @version 1.0
 */
public class App {
    /**
     *  main it's user
     * @param args from user
     */
    public static void main(final String[] args) {
        View view = new View();
        view.start();
    }
}
