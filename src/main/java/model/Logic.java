package model;

import model.Stone.PreciousStone;
import model.Stone.SemiPreciousStone;
import model.Stone.Stone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Collections;
import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * @version 1.0 Logic
 */
class Logic {
    private Map<String, Stone> allStones;
    private ArrayList<Stone> necklace = new ArrayList<>();

    Logic() {
        allStones = new HashMap<>();


        for(int i = 0; i < 7; i++)
        allStones.put("1", new PreciousStone(3, 8, "Diamond"));
        allStones.put("2", new PreciousStone(4, 7, "Agate"));
        allStones.put("3", new PreciousStone(3, 6, "Melanite"));
        allStones.put("4", new PreciousStone(5, 8, "Diopganite"));
        allStones.put("5", new PreciousStone(2, 5, "Kunzite"));
        allStones.put("6", new PreciousStone(5, 9, "Ruby"));
        allStones.put("7", new PreciousStone(3, 6, "Diamond"));

        allStones.put("8", new SemiPreciousStone(4, 1, "Amethyst"));
        allStones.put("9", new SemiPreciousStone(6, 2, "Charoite"));
        allStones.put("10", new SemiPreciousStone(3, 2, "Corundum"));
        allStones.put("11", new SemiPreciousStone(2, 1, "Taffeite"));
        allStones.put("12", new SemiPreciousStone(3, 3, "Topaz"));
        allStones.put("13", new SemiPreciousStone(4, 1, "Violet"));
    }

    /**
     * Field necklace
     */
    void pickStones() {

        String answer;
        Scanner scan = new Scanner(System.in);

        if (necklace.size() > 0) {
            necklace.clear();
        }
        do {
            int count = 0;
            System.out.println("Precious Stone:");
            for (int i = 0; i < allStones.size(); i++) {
                String getKey = (1 + i) + "";
                System.out.println(i + 1 + ") " + allStones.get(getKey));

                if (count == 0
                        && allStones.get(getKey) instanceof SemiPreciousStone) {
                    System.out.println("Semi-Precious Stone:");
                    count++;
                }

                if (i == allStones.size() - 1) {
                    System.out.println("Stop) exit from picking");
                }
            }

            System.out.print("\nEnter number of stone: ");
            answer = scan.nextLine().toUpperCase();
            try {
                if (allStones.get(answer) != null) {
                    necklace.add(allStones.get(answer));
                }
            } catch (Exception e) {
                System.out.println("Something went wrong");
            }
        } while (!answer.equals("STOP"));

        String st = necklace.size() > 1 ? " stones" : " stone";
        System.out.println("You picked " + necklace.size() + st);

    }

    /**
     * print info about necklace
     */
    void printInfoOfNecklace() {
        if (necklace.size() > 0) {
            for (Stone stone : necklace) {
                System.out.println(stone);
            }
        } else {
            System.out.println("You don't have stones\n"
                    + "Please press \'1\'");
        }
    }

    /**
     * Calculate all price of necklace
     */
    void calculatePriceOfStone() {
        double priceOfStones = 0;

        for (Stone stone : necklace) {
            priceOfStones += stone.getPrice();
        }

        System.out.println("Price of necklace: " + priceOfStones + "$");
    }

    /**
     * Sort all price of necklace
     */
    void sortPriceOfStones() {
        for (int j = 0; j < necklace.size(); j++) {
            int count = 0;

            for (int i = 1; i < necklace.size(); i++) {
                if (necklace.get(i).compareTo(necklace.get(i - 1)) > 0) {
                    Collections.swap(necklace, i, i - 1);
                    count++;
                }
            }

            if (count == 0) {
                break;
            }
        }

        infoOfSort(necklace);
    }

    /**
     * Sort by transparency
     */
    void sortTransparencyOfStones() {
        ArrayList<Stone> stonesOfSorting = new ArrayList<>(necklace);
        ArrayList<Stone> stonesOfSorting1 = new ArrayList<>();

        int start;
        int end;
        Scanner scanner = new Scanner(System.in);

        do {
            try {
                System.out.print("Enter the start of interval: ");
                start = scanner.nextInt();
                if (start >= 0) {
                    break;
                } else {
                    System.out.println("Start of transparency must be more than "
                            + start);
                }
            } catch (InputMismatchException e) {
                System.out.println("\nError. Try again.\n");
                scanner = new Scanner(System.in);
            }
        } while (true);
        do {
            try {
                System.out.print("Enter the end of interval: ");
                end = scanner.nextInt();
                if (end > start) {
                    break;
                } else {
                    System.out.println("End of interval must be more than start interval("
                            + start + ")"
                            + "\nPlease, try again.\n");
                }
            } catch (InputMismatchException e) {
                System.out.println("\nError. Try again.\n");
                scanner = new Scanner(System.in);
            }
        } while (true);

        for (int i = 0; i < stonesOfSorting.size(); i++) {
            if (stonesOfSorting.get(i).compareTo(start, end)) {
                stonesOfSorting1.add(stonesOfSorting.get(i));
            }
            stonesOfSorting.remove(i--);
        }

        infoOfSort(stonesOfSorting1);
    }

    /**
     * @param stoneArrayList arrayList for print info
     */
    private void infoOfSort(final ArrayList<Stone> stoneArrayList) {
        for (Stone stone : stoneArrayList) {
            System.out.println(stone);
        }
    }
}
