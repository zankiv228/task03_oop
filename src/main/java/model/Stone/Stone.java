package model.Stone;

/**
 * It's the main class of Stones
 */
public class Stone implements Comparable<Stone> {
    private static final double PRICE_OF_CARAT = 900;
    private static final double GR_FOR_CARAT = 0.2;
    private static final int PERCENT = 100;

    private String name;
    private int carat;
    private double price;
    private int transparency;

    @Override
    public int compareTo(final Stone s) {
        if (this.getPrice() > s.getPrice()) {
            return 1;
        } else if (this.getPrice() < s.getPrice()) {
            return -1;
        }

        return 0;
    }

    public boolean compareTo(final int tpStart, final int tpEnd) {
        return this.getTransparency() >= tpStart
                && this.getTransparency() <= tpEnd;
    }

    /**
     *
     * @param carat carat of stone
     * @param transparency transparency of stone
     * @param name name of stone
     */
    Stone(final int carat, final int transparency, final String name) {
        this.name = name;
        this.carat = carat;
        this.transparency = transparency;

        price = (double) (Math.round(carat * GR_FOR_CARAT * PRICE_OF_CARAT * PERCENT)
                / PERCENT);
    }

    public int getCarat() {
        return carat;
    }

    public double getPrice() {
        return price;
    }

    public int getTransparency() {
        return transparency;
    }

    @Override
    public String toString() {
        return name
                + " — carat=" + carat
                + ", price=" + price + "$"
                + ", transparency=" + transparency;
    }
}
