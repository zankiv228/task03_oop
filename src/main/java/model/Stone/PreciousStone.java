package model.Stone;

/**
 * @version 1.0 PreciousStone
 */
public class PreciousStone extends Stone {
    public PreciousStone(final int carat, final int transparency, final String name) {
        super(carat, transparency, name);
    }
}
