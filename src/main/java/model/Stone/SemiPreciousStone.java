package model.Stone;

/**
 * @version 1.0 SemiPreciousStone
 */
public class SemiPreciousStone extends Stone {
    public SemiPreciousStone(final int carat, final int transparency, final String name) {
        super(carat, transparency, name);
    }
}
