package model;

/**
 * Manipulations with stones
 */
public interface Model {

    void pickStones();

    void printInfoOfNecklace();

    void calculatePriceOfStone();

    void sortPriceOfStones();

    void sortTransparencyOfStones();
}
