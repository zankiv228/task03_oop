package model;

/**
 * @version 1.0 BusinessLogic
 */
public class BusinessLogic implements Model {
    private Logic logic = new Logic();

    @Override
    public void pickStones() {
        logic.pickStones();
    }

    @Override
    public void printInfoOfNecklace() {
        logic.printInfoOfNecklace();
        System.out.println("           @%          %@\n"
                + "         .%@            @%.\n"
                + "        .@###.        .###@.\n"
                + "         :####..    ..####:\n"
                + "          -=@@@......@@@=-\n"
                + "             -========-\n"
                + "            -==========-\n"
                + "             -========-\n"
                + "                 ==\n"
                + "                 ##\n"
                + "                @@@@\n"
                + "                 @@");
    }

    @Override
    public void calculatePriceOfStone() {
        logic.calculatePriceOfStone();
    }

    @Override
    public void sortPriceOfStones() {
        logic.sortPriceOfStones();
    }

    @Override
    public void sortTransparencyOfStones() {
        logic.sortTransparencyOfStones();
    }
}
