package controller;

public interface Controlable {
    void pickStones();
    void printInfoOfNecklace();
    void calculatePrices();
    void sortPriceOfStones();
    void sortByTransparency();
}
