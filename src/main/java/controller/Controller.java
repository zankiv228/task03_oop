package controller;

import model.BusinessLogic;

public class Controller implements Controlable {
    private BusinessLogic logic;

    public Controller() {
        logic = new BusinessLogic();
    }

    @Override
    public void pickStones() {
        logic.pickStones();
    }

    @Override
    public void printInfoOfNecklace() {
        logic.printInfoOfNecklace();
    }

    @Override
    public void calculatePrices() {
        logic.calculatePriceOfStone();
    }

    @Override
    public void sortPriceOfStones() {
        logic.sortPriceOfStones();
    }

    @Override
    public void sortByTransparency() {
        logic.sortTransparencyOfStones();
    }
}
